isort src
black src 
flake8 --ignore=E203,W503 src
find src -type f -name "*.py" | xargs pylint
