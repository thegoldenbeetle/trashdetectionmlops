import random
from dataclasses import dataclass
from pathlib import Path

import cv2
import pandas as pd
import torch
import torchvision.transforms as T
import torchvision.transforms.functional as F
from PIL import Image
from torch.utils.data import Dataset
from torch.utils.data.dataloader import default_collate
from torchvision.io import read_image
from torchvision.utils import draw_segmentation_masks

from src.tools import rle_decode

MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]
ANGLES = [0, 90, 180, 270]


@dataclass
class TrashDatasetItem:
    image: torch.Tensor
    normalize_image: torch.Tensor
    trash_mask: torch.Tensor
    image_path: Path

    def to_list_to_train(self):
        return [self.image, self.normalize_image, self.trash_mask]

    def show(self, alpha=0.7) -> Image:
        print(self.image_path)
        return F.to_pil_image(
            draw_segmentation_masks(
                self.image, self.trash_mask.bool(), alpha=alpha
            )
        )

    def apply_func(self, func):
        res_image = func(self.image)
        res_trash_mask = func(self.trash_mask)
        res_normalize_image = func(self.normalize_image)
        return TrashDatasetItem(
            res_image, res_normalize_image, res_trash_mask, self.image_path
        )

    def resize(self, new_size):
        return self.apply_func(T.Resize(size=new_size, antialias=True))

    def hflip(self):
        return self.apply_func(F.hflip)

    def vflip(self):
        return self.apply_func(F.vflip)

    def rotate(self, angle):
        return self.apply_func(lambda x: F.rotate(x, angle))

    def crop(self, crop_params):
        return self.apply_func(lambda x: T.functional.crop(x, *crop_params))

    @staticmethod
    def merge(items, cat_indx=1):
        result_image = torch.cat(tuple(x.image for x in items), cat_indx)
        result_normalize_image = torch.cat(
            tuple(x.normalize_image for x in items), cat_indx
        )
        result_trash_mask = torch.cat(
            tuple(x.trash_mask for x in items), cat_indx
        )
        result_image_path = str([x.image_path for x in items])
        return TrashDatasetItem(
            result_image,
            result_normalize_image,
            result_trash_mask,
            result_image_path,
        )


def trash_collate(data):
    data_list = [x.to_list_to_train() for x in data]
    return default_collate(data_list)


class TrashDataset(Dataset):
    def __init__(
        self,
        csv_file,
        images_path,
        masks_path=None,
        image_size=(256, 256),
        to_transform=False,
    ):
        self.annotations = pd.read_csv(csv_file)
        self.images_path = images_path
        self.masks_path = masks_path
        self.to_transform = to_transform
        self.image_size = image_size

    def __len__(self):
        return len(self.annotations)

    # original size, without any transforms
    def get_item(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        # read image
        image_filename = self.annotations.loc[idx, "file_name"]
        image_path = self.images_path / image_filename
        image = read_image(str(image_path))
        _, height, width = image.shape

        # load mask
        if self.masks_path is not None:
            trash_mask = cv2.imread(
                str(self.masks_path / (image_filename[:-4] + ".png")),
                cv2.IMREAD_GRAYSCALE,
            )
            trash_mask = (trash_mask != 0).astype(int)
        else:
            rle = self.annotations.loc[idx, "rle"]
            trash_mask = rle_decode(rle, (height, width))

        trash_mask = torch.from_numpy(trash_mask).float()
        trash_mask = torch.unsqueeze(trash_mask, 0)

        normalize_image = T.Normalize(mean=MEAN, std=STD)(image.float())

        return TrashDatasetItem(image, normalize_image, trash_mask, image_path)

    def get_validation_item(self, idx):
        item = self.get_item(idx)

        if self.image_size is not None:
            item = item.resize(self.image_size)

        return item

    # random crop for every image, resize to the same size, rotates/flips
    def ordinar_transform(self, item):
        orig_h, orig_w = item.image.shape[1:]
        new_w = random.randint(int(0.7 * orig_w), orig_w)
        new_h = random.randint(int(0.7 * orig_h), orig_h)
        cropper_1 = T.RandomCrop(size=(new_h, new_w))
        crop1_params = cropper_1.get_params(item.image, (new_h, new_w))
        item = item.crop(crop1_params)
        item = item.resize(self.image_size)
        if random.random() > 0.5:
            item = item.hflip()
        if random.random() > 0.5:
            item = item.vflip()
        angle = random.choice(ANGLES)
        item = item.rotate(angle)
        return item

    def get_train_item(self):
        rand_int = random.choice([1, 2, 4])
        temp_df = self.annotations.sample(n=rand_int, replace=True)

        indexes = temp_df.index.to_list()

        items = [self.get_item(ind) for ind in indexes]
        items = [self.ordinar_transform(item) for item in items]

        if rand_int == 1:
            return items[0]
        if rand_int == 2:
            merged_item = TrashDatasetItem.merge(items)
        elif rand_int == 4:
            merged_item_1 = TrashDatasetItem.merge(items[:2])
            merged_item_2 = TrashDatasetItem.merge(items[2:])
            merged_item = TrashDatasetItem.merge(
                [merged_item_1, merged_item_2], 2
            )

        cropper = T.RandomCrop(size=self.image_size)
        crop_params = cropper.get_params(merged_item.image, self.image_size)
        res_item = merged_item.crop(crop_params)
        return res_item

    def __getitem__(self, idx):
        if self.to_transform:
            return self.get_train_item()

        return self.get_validation_item(idx)

    def show_item(self, idx, alpha=0.7):
        item = self[idx]
        return F.to_pil_image(
            draw_segmentation_masks(
                item["image"], masks=item["trash_mask"].bool(), alpha=alpha
            )
        )
