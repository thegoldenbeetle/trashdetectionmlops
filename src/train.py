import glob
import tempfile

import mlflow
import onnx
import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks import ModelCheckpoint
from torch.utils.data import DataLoader

from src.constants import EXPERIMENT_NAME, TRACKING_URL
from src.dataset import TrashDataset, trash_collate
from src.model import unet_model
from src.settings import (
    BATCH_SIZE,
    IMAGE_SIZE,
    IMAGES_PATH,
    MASKS_PATH,
    TRAIN_ANNOTATIONS_PATH,
    VAL_ANNOTATIONS_PATH,
)
from src.trash_trainer import TrashTrainer

pl.seed_everything(42)


if __name__ == "__main__":
    train_dataset = TrashDataset(
        TRAIN_ANNOTATIONS_PATH,
        IMAGES_PATH,
        MASKS_PATH,
        image_size=IMAGE_SIZE,
        to_transform=True,
    )
    val_dataset = TrashDataset(
        VAL_ANNOTATIONS_PATH,
        IMAGES_PATH,
        MASKS_PATH,
        image_size=IMAGE_SIZE,
    )

    train_dataloader = DataLoader(
        train_dataset,
        collate_fn=trash_collate,
        batch_size=BATCH_SIZE,
        shuffle=True,
        num_workers=6,
    )
    val_dataloader = DataLoader(
        val_dataset,
        collate_fn=trash_collate,
        batch_size=BATCH_SIZE,
        shuffle=False,
        num_workers=6,
    )

    test_image = val_dataset[0]
    trash_model = TrashTrainer(unet_model, test_image)

    mlflow.set_tracking_uri(TRACKING_URL)
    mlflow.set_experiment(EXPERIMENT_NAME)
    mlflow.pytorch.autolog()
    mlflow.start_run()

    # pylint: disable=R1732
    temp_dir = tempfile.TemporaryDirectory()

    checkpoint_callback = ModelCheckpoint(
        save_top_k=1, monitor="Val loss", mode="min", dirpath=temp_dir.name
    )

    trainer = pl.Trainer(
        devices=1,
        accelerator="gpu",
        max_epochs=20,
        callbacks=[checkpoint_callback],
    )

    trainer.fit(
        model=trash_model,
        train_dataloaders=train_dataloader,
        val_dataloaders=val_dataloader,
    )

    saved_models_paths = glob.glob(f"{temp_dir.name}/*.ckpt")
    mlflow.log_artifact(saved_models_paths[0])
    best_model = TrashTrainer.load_from_checkpoint(
        saved_models_paths[0], model=unet_model
    )
    best_model = best_model.eval()
    torch.onnx.export(
        best_model,
        torch.randn(1, 3, *IMAGE_SIZE),
        f"{temp_dir.name}/best_model.onnx",
        export_params=True,
        opset_version=11,
        input_names=["input"],
        output_names=["output"],
    )
    onnx_model = onnx.load(f"{temp_dir.name}/best_model.onnx")
    mlflow.onnx.log_model(onnx_model, artifact_path="model_onnx")

    temp_dir.cleanup()
