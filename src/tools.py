import time

import numpy as np
import torch


def get_sync_time():
    if torch.cuda.is_available():
        torch.cuda.synchronize()
    return time.perf_counter()


def rle_encode(img):
    """
    img: numpy array, 1 - mask, 0 - background
    Returns run length as string formated
    """
    pixels = img.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return " ".join(str(x) for x in runs)


def rle_decode(mask_rle, shape):
    """
    mask_rle: run-length as string formated (start length)
    shape: (height,width) of array to return
    Returns numpy array, 1 - mask, 0 - background

    """
    rle = mask_rle.split()
    starts, lengths = [
        np.asarray(x, dtype=int) for x in (rle[0:][::2], rle[1:][::2])
    ]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0] * shape[1], dtype=np.uint8)
    for lo_, hi_ in zip(starts, ends):
        img[lo_:hi_] = 1
    return img.reshape(shape)


SMOOTH = 1e-6


def iou_pytorch(outputs: torch.Tensor, labels: torch.Tensor):
    outputs = outputs.squeeze(1)
    labels = labels.squeeze(1)

    outputs = outputs > 0.5
    labels = labels.bool()

    intersection = (outputs & labels).float().sum((1, 2))
    union = (outputs | labels).float().sum((1, 2))

    iou = (intersection + SMOOTH) / (union + SMOOTH)

    return iou.mean()


def dsc_pytorch(outputs: torch.Tensor, labels: torch.Tensor):
    outputs = outputs.squeeze(1)
    labels = labels.squeeze(1)

    outputs = outputs > 0.5
    labels = labels.bool()

    intersection = (outputs & labels).float().sum((1, 2))
    outputs = outputs.float().sum((1, 2))
    labels = labels.float().sum((1, 2))

    dsc = (2 * intersection + SMOOTH) / (outputs + labels + SMOOTH)

    return dsc.mean()
