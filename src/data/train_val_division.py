import argparse

import pandas as pd

from src.constants import FILE_NAME, RLE
from src.settings import (
    ANNOTATIONS_PATH,
    TRAIN_ANNOTATIONS_PATH,
    VAL_ANNOTATIONS_PATH,
)

VAL_IDS = (18, 24, 63, 89, 64)
TRAIN_IDS = (
    37,
    80,
    75,
    71,
    34,
    49,
    60,
    13,
    27,
    97,
    79,
    42,
    102,
    265,
    83,
    22,
    32,
    17,
    8,
)


def parse_args():
    parser = argparse.ArgumentParser(
        prog="Prepare data",
        description="Generate annotations file for training based \
            on raw annotations file all.csv",
    )
    parser.add_argument("-a", "--annotations", default=ANNOTATIONS_PATH)
    return parser.parse_args()


def get_seq_id(filepath):
    filename = filepath.split("/")[-1]
    seq_id = int(filename[filename.find("_") + 1 : filename.find("-")])
    return seq_id


if __name__ == "__main__":
    args = parse_args()

    df = pd.read_csv(args.annotations)
    df["seq_id"] = [
        int(x[x.find("_") + 1 : x.find("-")]) for x in df[FILE_NAME]
    ]

    train_data = df[df["seq_id"].isin(TRAIN_IDS)].loc[:, [FILE_NAME, RLE]]
    val_data = df[df["seq_id"].isin(VAL_IDS)].loc[:, [FILE_NAME, RLE]]

    train_data.to_csv(TRAIN_ANNOTATIONS_PATH, index=False)
    val_data.to_csv(VAL_ANNOTATIONS_PATH, index=False)
