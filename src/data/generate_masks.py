import argparse
from pathlib import Path

import cv2
import pandas as pd
from torchvision.io import read_image
from tqdm import tqdm

from src.constants import FILE_NAME, RLE
from src.settings import ANNOTATIONS_PATH, IMAGES_PATH, MASKS_PATH
from src.tools import rle_decode  # noqa: E402  # pylint: disable=C0413


def parse_args():
    parser = argparse.ArgumentParser(
        prog="Generate masks",
        description="Generate masks by annotations \
            contating them in rle format",
    )
    parser.add_argument("-i", "--input", default=ANNOTATIONS_PATH)
    parser.add_argument("-o", "--output", default=MASKS_PATH)
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    Path(args.output).mkdir(parents=True, exist_ok=True)

    df = pd.read_csv(args.input)
    for index, row in tqdm(df.iterrows(), total=len(df)):
        image_file_name = row[FILE_NAME]
        image_path = IMAGES_PATH / image_file_name
        image = read_image(str(image_path))
        _, height, width = image.shape
        rle_decoded = rle_decode(row[RLE], (height, width))
        result = cv2.imwrite(
            str(args.output / (image_file_name[:-4] + ".png")),
            rle_decoded * 255,
        )
        if not result:
            print(image_file_name, result)
