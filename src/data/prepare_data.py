import argparse

import pandas as pd

from src.constants import FILE_NAME, RAW_INPUT_IMAGE, RLE
from src.settings import ANNOTATIONS_PATH, DATA_ROOT


def parse_args():
    parser = argparse.ArgumentParser(
        prog="Prepare data",
        description="Generate annotations file for training based \
            on raw annotations file all.csv",
    )
    parser.add_argument("-i", "--input", default=DATA_ROOT / "all.csv")
    parser.add_argument("-o", "--output", default=ANNOTATIONS_PATH)
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    df = pd.read_csv(args.input)
    df[FILE_NAME] = [x.split("/")[-1] for x in df[RAW_INPUT_IMAGE]]

    result = {FILE_NAME: [], RLE: []}
    for index, row in df.iterrows():
        result[FILE_NAME].append(row[FILE_NAME])
        result[RLE].append(row[RLE])

    df = pd.DataFrame(result)
    df.to_csv(args.output, index=False)
