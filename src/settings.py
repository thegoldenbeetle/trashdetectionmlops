from pathlib import Path

# data paths
DATA_ROOT = Path("./data")
IMAGES_PATH = DATA_ROOT / "train"
MASKS_PATH = DATA_ROOT / "masks"
ANNOTATIONS_PATH = DATA_ROOT / "all_prepared.csv"
TRAIN_ANNOTATIONS_PATH = DATA_ROOT / "train.csv"
VAL_ANNOTATIONS_PATH = DATA_ROOT / "val.csv"

# dataloader params
IMAGE_SIZE = (384, 384)
BATCH_SIZE = 8
