import pytorch_lightning as pl
import torch
from torch import nn
from torchvision.utils import draw_segmentation_masks

from src.tools import dsc_pytorch, get_sync_time, iou_pytorch


class TrashTrainer(pl.LightningModule):
    def __init__(self, model, test_image_item=None):
        super().__init__()
        self.model = model
        self.loss_function = nn.BCELoss()
        self.test_image = test_image_item

    def forward(self, x):  # pylint: disable=W0221
        return self.model(x)

    def training_step(self, batch, batch_idx):  # pylint: disable=W0221, W0613
        _, images, masks = batch
        predictions = self.model(images)
        train_loss = self.loss_function(predictions, masks)
        self.log("Train loss", train_loss)
        return train_loss

    def validation_step(
        self, batch, batch_idx
    ):  # pylint: disable=W0221, W0613
        _, images, masks = batch
        predictions = self.model(images)
        val_loss = self.loss_function(predictions, masks)
        iou = iou_pytorch(predictions, masks)
        self.log("Val loss", val_loss)
        self.log("IoU", iou)

        if self.test_image:
            test_prediction = self.model(
                torch.unsqueeze(self.test_image.normalize_image, 0).cuda()
            )
            tensorboard = self.logger.experiment
            tensorboard.add_image(
                "Val image",
                draw_segmentation_masks(
                    self.test_image.image.cpu(),
                    (test_prediction[0] > 0.5).cpu(),
                    alpha=0.7,
                ),
                self.global_step,
            )

    def test_step(self, batch, batch_idx):  # pylint: disable=W0221, W0613
        _, images, masks = batch

        start_time = get_sync_time()
        predictions = self.model(images)
        end_time = get_sync_time()

        iou = iou_pytorch(predictions, masks)
        dsc = dsc_pytorch(predictions, masks)
        self.log("IoU", iou)
        self.log("DSC", dsc)
        self.log("Time", end_time - start_time)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer
