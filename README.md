# Trash Detection Project

Проект по распознаванию несанкционированных мусорных свалок с помощью моделей глубокого обучения по фотографиям, полученным с квадрокоптеров.

![](./imgs/scheme.png)

Цель проекта: реализовать решение, осуществляющее семантическую сегментацию свалок на изображениях.

Примеры изображений и результата: 

|  ![](./imgs/DJI_0008-700.jpg)    |  ![](./imgs/DJI_0027-475.jpg)    |  ![](./imgs/DJI_0060-2725.jpg)    |  ![](./imgs/DJI_0089-1200.jpg)    |
| ---- | ---- | ---- | ---- |
|  ![](./imgs/example1.jpg)    |  ![](./imgs/example2.jpg)    |  ![](./imgs/example3.jpg)    |  ![](./imgs/example4.jpg)    |

Исходный [датасет](https://www.kaggle.com/competitions/drone-garbage/data) был предоставлен организацией "Чистая Вуокса".

В качестве метрики качества результата используется IOU, требуется IOU >=0.5 на тестовой части данных, предоставленных заказчиком.

## Настройка среды

Необходимые утилиты и пакеты:
- git
- python3
- poetry

Для настройки среды следует установить необходимые пакеты, затем выполнить следующие команды:
1. git clone git@gitlab.com:thegoldenbeetle/trashdetectionmlops.git
2. cd trashdetectionmlops
3. Получить и прописать необходимые ключи от используемых хранилищей
4. make set_environment

## Запуск процесса обучения

1. make run_training

## Структура проекта

    ├── .dvc                                <- DVC зависимости
    |── doc                                 <- Директория для хранения документации по проекту 
    |── data                                <- Директория для хранения данных, используемых в проекте 
    |── docker                              <- Директория, содержащая docker файлы для развертывания на сервере 
    ├── notebooks                           <- Jupyter notebooks для быстрых проверок гипотез
    │   ├── dataset_visualization.ipynb
    │   └── mask_visualization.ipynb
    ├── src                                 <- Исходный код проекта
    │   ├── data                            <- Скрипты, используемые для обработки данных
    │   │   ├── generate_masks.py
    │   │   ├── prepare_data.py
    │   │   └── train_val_division.py
    │   ├── __init__.py
    │   ├── constants.py
    │   ├── dataset.py
    │   ├── model.py
    │   ├── settings.py                      
    │   ├── tools.py
    │   ├── train.py                        <- Скрипт, являющийся входной точкой обучения
    │   └── trash_trainer.py
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── dvc.lock
    ├── dvc.yaml
    ├── fix_codestyle.sh                    <- Скрипт для исправления и проверки codestyle (pep 8)
    ├── makefile                            <- Makefile для быстрой настройки среды и запуска процесса обучения
    ├── poetry.lock        
    ├── pyproject.toml     
    └── README.md

## Проведенные эксперименты

|             Модель                        |          Train loss                       |          Val loss                  |                      Val Iou                      |
| ----------------------------------------- | ----------------------------------------- | -------------------------------------- | ------------------------ |
| Unet с resnet34 предобученная на imagenet | ![](./imgs/unet_train_loss_pretrained.png) |![](./imgs/unet_val_loss_pretrained.png) | ![](./imgs/unet_iou_pretrained.png) |





